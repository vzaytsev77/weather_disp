#ifndef CCONFIGFILEOBJECT_H
#define CCONFIGFILEOBJECT_H

#include <vector>
#include <initializer_list>
#include <algorithm>
#include <errno.h>
#include <limits.h>
#include "log.h"



class CConfigFileObject
{
public:
    // ctor
    CConfigFileObject();
    // ctor и сразу загрузка файла
    CConfigFileObject( const char * a_config_file_name );
    // dtor
    virtual ~CConfigFileObject();
    //
    bool parse_file( const char * a_config_file_name );

    //
    typedef std::vector<long> indices_t;
    //
    struct SParameter
    {
        // незвание параметра
        const char * m_name;
        // индексы
        indices_t m_indices;
        // значение параметра
        const char * m_value;
    };

    typedef std::vector<SParameter> params_t;

    struct SSection
    {
        // название секции
        const char * m_name;
        // параметры в секции
        params_t m_params;
    };

    typedef std::vector<SSection> sections_t;

    typedef std::pair<params_t::const_iterator, params_t::const_iterator> param_entries_t;

    //
    bool get_param_entries(CConfigFileObject::param_entries_t & a_dst, const char *a_section, const char *a_param) const;
    //
    template<class TDestination, class TIndices = std::initializer_list<long>>
    bool get_string( TDestination & a_dest, const char * a_section, const char * a_param, TIndices && a_indices = {} ) const
    {
        CConfigFileObject::SSection v_section;

        //
        v_section.m_name = a_section;
        //
        std::pair<sections_t::const_iterator, sections_t::const_iterator> v_section_it
                = std::equal_range(
                    m_data.cbegin()
                    , m_data.cend()
                    , v_section
                    );
        //
        if ( v_section_it.first != v_section_it.second )
        {
            CConfigFileObject::SParameter v_param;

            //
            v_param.m_name = a_param;
            //
            for ( const long & i: a_indices )
            {
                //
                v_param.m_indices.push_back( i );
            }
            //
            param_entries_t v_param_it
                    = std::equal_range(
                        v_section_it.first->m_params.cbegin()
                        , v_section_it.first->m_params.cend()
                        , v_param
                        );
            //
            if ( v_param_it.first != v_param_it.second )
            {
                //
                a_dest = v_param_it.first->m_value;
                //
                return true;
            }
            else
            {
                //
                log_error(
                            "CONF: parameter [%s]%s with %u indices not found"
                            , a_section
                            , a_param
                            , v_param.m_indices.size()
                            );
                //
                return false;
            }
        }
        else
        {
            //
            log_error(
                        "CONF[%s:%d]: section %s not found"
                        , __FILE__
                        , __LINE__
                        , a_section
                        );
            //
            return false;
        }
    }


    template<class TDestination, class TIndices = std::initializer_list<long>>
    bool get_long( TDestination & a_dst, const char * a_section, const char * a_param, TIndices && a_indices = {} ) const
    {
        CConfigFileObject::SSection v_section;

        //
        v_section.m_name = a_section;
        //
        std::pair<sections_t::const_iterator, sections_t::const_iterator> v_section_it
                = std::equal_range(
                    m_data.cbegin()
                    , m_data.cend()
                    , v_section
                    );
        //
        if ( v_section_it.first != v_section_it.second )
        {
            CConfigFileObject::SParameter v_param;

            //
            v_param.m_name = a_param;
            //
            for ( const long & i: a_indices )
            {
                //
                v_param.m_indices.push_back( i );
            }
            //
            param_entries_t v_param_it
                    = std::equal_range(
                        v_section_it.first->m_params.cbegin()
                        , v_section_it.first->m_params.cend()
                        , v_param
                        );
            //
            if ( v_param_it.first != v_param_it.second )
            {
                //
                a_dst = ::strtol( v_param_it.first->m_value, 0, 0 );
                //
                if ((ERANGE == errno && (LONG_MAX == (long)a_dst || LONG_MIN == (long)a_dst))
                                   || (errno != 0 && 0 == a_dst))
                {
                    //
                    log_error(
                                "CONF: [%s]%s = \"%s\" seems not a long int"
                                , a_section
                                , a_param
                                , v_param_it.first->m_value
                                );
                    //
                    return false;
                }
                else
                {
                    //
                    return true;
                }
            }
            else
            {
                //
                log_error(
                            "CONF: parameter [%s]%s with %u indices not found"
                            , a_section
                            , a_param
                            , v_param.m_indices.size()
                            );
                //
                return false;
            }
        }
        else
        {
            //
            log_error(
                        "CONF[%s:%d]: section %s not found"
                        , __FILE__
                        , __LINE__
                        , a_section
                        );
            //
            return false;
        }
    }

protected:
    // место, где сложен весь файл
    char * m_config_file;
    // содержимое файла
    sections_t m_data;
    // зачистка
    void cleanup();
};


bool operator < ( const CConfigFileObject::SSection & a_1, const CConfigFileObject::SSection & a_2 );
bool operator < ( const CConfigFileObject::SParameter & a_1, const CConfigFileObject::SParameter & a_2 );


#endif // CCONFIGFILEOBJECT_H
