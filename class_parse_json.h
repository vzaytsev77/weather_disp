#ifndef CLASS_PARSE_JSON
#define CLASS_PARSE_JSON

#include <cstdlib>
#include <unistd.h>
#include <signal.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <string.h>
#include <string>
#include <map>
#include <limits.h>
#include <jsoncpp/json/json.h>
#include <pthread.h>



#define PARSE_JSON_VALUE_DEFAULT_NAME     "default"


class CParseJson
{
public:
    struct SParseJsonDesc
    {
        const Json::Value * m_value;
        Json::ValueType m_value_type;
        const char * m_name;
        const char * m_default_value;
    };

    struct SParseJsonDescMeta
    {
        bool m_field_names_sorted;
        pthread_mutex_t m_mutex;
        Json::Value m_json_root;
        //
        SParseJsonDescMeta();
        //
        virtual ~SParseJsonDescMeta();
    };

    //
    CParseJson(
            SParseJsonDesc * a_json_desc
            , size_t a_desc_count
            , SParseJsonDescMeta * a_desc_meta
            );
    virtual ~CParseJson();
    //
    bool get_json_values( Json::Value & a_json );
    bool get_json_values( const char * a_json_text );
    //
    const Json::Value * operator [] ( const char * a_field_name ) const;
protected:
    SParseJsonDesc *     m_json_desc;
    size_t               m_desc_count;
    SParseJsonDescMeta * m_desc_meta;
    Json::Reader         m_json_reader;
};


// пример использования
//  поддерживаются типы значений:
//  Json::objectValue
//  Json::arrayValue
//  Json::stringValue
//  Json::intValue
//
//  пример со значениями по-умолчанию
//  , {0, Json::stringValue, "externalId"   , "some string"}
//  , {0, Json::intValue   , "merchant"     , "1034"}
//  , {0, Json::arrayValue , "paymentSystem", "{\"default\":[1,4,3]}"
//  , {0, Json::objectValue, "productId"    , "{\"default\":{\"dummy\":155}}"
//  , {0, Json::arrayValue , "solarSystem"  , "{}"
//  , {0, Json::objectValue, "contactId"    , "{}"
//
//
//    // эмулятор бонус-сервера...
//    CParseJson::SParseJsonDescMeta g_parse_bonus_server_meta;
//    CParseJson::SParseJsonDesc g_parse_bonus_server[] =
//    {
//        {0, Json::stringValue, "accountId"    , 0} // здесь везде значения по-умолчанию не заданы
//      , {0, Json::intValue   , "amount"       , 0} // то есть все поля должны присутствовать
//      , {0, Json::stringValue, "externalId"   , 0}
//      , {0, Json::stringValue, "merchant"     , 0}
//      , {0, Json::stringValue, "paymentSystem", 0}
//      , {0, Json::stringValue, "productId"    , 0}
//      , {0, Json::intValue   , "time"         , 0}
//    };
//
//    int reply_fn_bonus_server( const CString & a_request, CString & a_content_type, CString & a_reply_body )
//    {
//        Json::Value v_root;
//        Json::Reader v_reader;
//
//        //
//        a_content_type = "application/json";
//        a_reply_body.clear();
//        //
//        if ( v_reader.parse( a_request.c_str(), v_root, false ) )
//        {
//            Json::Value v_reply;
//            CParseJson v_pj( g_parse_bonus_server, ARRAY_LEN(g_parse_bonus_server), &g_parse_bonus_server_meta );
//
//            //
//            if ( v_pj.get_json_values( v_root ) )
//            {
//                //
//                v_reply["externalId"] = *v_pj["externalId"];
//                v_reply["accountId"] = *v_pj["accountId"];
//                v_reply["amount"] = *v_pj["amount"];
//                v_reply["paymentSystem"] = *v_pj["paymentSystem"];
//                v_reply["merchant"] = *v_pj["merchant"];
//                v_reply["productId"] = *v_pj["productId"];
//                v_reply["error"] = Json::Value(Json::nullValue);
//                v_reply["endTime"] = v_pj["time"]->asInt64() * 1000;
//                v_reply["points"] = Json::Value(0);
//                v_reply["ruleData"] = Json::Value(Json::nullValue);
//                v_reply["status"] = Json::Value(2);
//                //
//                Json::FastWriter v_writer;
//                //
//                a_reply_body = v_writer.write( v_reply ).c_str();
//                //
//                return 200;
//            }
//            else
//            {
//                //
//                return 400;
//            }
//        }
//        else
//        {
//            //
//            return 400;
//        }
//    }






#endif // CLASS_PARSE_JSON

