# README #

This README would normally document whatever steps are necessary to get your application up and running.


### What is this repository for? ###

weather display based on openweathermap.org


### How do I get set up? ###

hardware:
Orange Pi Zero
ILI9341 display (320x240) via SPI (https://kaspars.net/blog/linux/spi-display-orange-pi-zero, use 9341 instead 9340)

software:
Armbian 5.X
libpng
libcurl
SDL (1.2) builded with fbcon video driver
SDL_image (1.2) builded with png support


build:
TOOLS=/home/vzaytsev/tools/armhf/ make -j4


start:
sudo SDL_FBDEV=/dev/fb8 ./weather_disp weather_disp.conf
