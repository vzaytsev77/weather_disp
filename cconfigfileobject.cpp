#include "cconfigfileobject.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>


//
bool is_utf8_part( int a_c )
{
    return  0x80 == (0xC0 & a_c)
         || 0xC0 == (0xE0 & a_c)
         || 0xE0 == (0xF0 & a_c)
         || 0xF0 == (0xF8 & a_c)
         || 0xF8 == (0xFC & a_c)
         || 0xFC == (0xFE & a_c)
         ;
}



// ctor
CConfigFileObject::CConfigFileObject()
    : m_config_file(0)
{
}


// ctor и сразу загрузка файла
CConfigFileObject::CConfigFileObject(const char *a_config_file_name)
    : m_config_file(0)
{
    //
    if ( ! parse_file( a_config_file_name ) )
    {
        //
        cleanup();
    }
}


// dtor
CConfigFileObject::~CConfigFileObject()
{
    //
    cleanup();
}


//
void CConfigFileObject::cleanup()
{
    //
    m_data.clear();
    //
    if ( m_config_file )
    {
        //
        delete [] m_config_file;
        //
        m_config_file = 0;
    }
}


#define ST_UNKNOWN              -1
#define ST_OUTSIDE_SECTION      0
#define ST_READ_SECTION_NAME    1
#define ST_INSIDE_SECTION       2
#define ST_READ_PARAM_NAME      3
#define ST_WAIT_INDEX_START     4
#define ST_READ_INDEX           5
#define ST_READ_INDEX_DIGITS    6
#define ST_WAIT_PARAM_START     7
#define ST_READ_PARAM_VALUE     8
#define ST_SKIP_COMMENT         9


#define LEFT_SECTION_BRACKET    '['
#define RIGHT_SECTION_BRACKET   ']'
#define LEFT_INDEX_BRACKET      '['
#define RIGHT_INDEX_BRACKET     ']'
#define PARAM_START_MARKER      '='
#define PARAM_START_STRING      "\"'"
#define COMMENTS_START          "#/"

bool operator < ( const CConfigFileObject::SSection & a_1, const CConfigFileObject::SSection & a_2 )
{
    //
    return ::strcmp( a_1.m_name, a_2.m_name ) < 0;
}


bool operator < ( const CConfigFileObject::SParameter & a_1, const CConfigFileObject::SParameter & a_2 )
{
    //
    int v_rc = ::strcmp( a_1.m_name, a_2.m_name );
    //
    if ( 0 == v_rc )
    {
        // теперь сравниваем индексы
        if ( a_1.m_indices.size() == a_2.m_indices.size() )
        {
            //
            for ( size_t i = 0; i < a_1.m_indices.size(); ++i )
            {
                //
                if ( a_1.m_indices.at(i) != a_2.m_indices.at(i) )
                {
                    //
                    return a_1.m_indices.at(i) < a_2.m_indices.at(i);
                }
            }
            //
            return false;
        }
        else
        {
            //
            return a_1.m_indices.size() < a_2.m_indices.size();
        }
    }
    else
    {
        //
        return v_rc < 0;
    }
}


//
bool CConfigFileObject::parse_file(const char *a_config_file_name)
{
    int v_line_number = 1;

    //
    cleanup();
    //
    struct stat v_stat;
    ::bzero( &v_stat, sizeof(v_stat) );
    // проверяем наличие файла
    if ( 0 != ::stat( a_config_file_name, &v_stat ) )
    {
        //
        log_error( "CONF: can't stat file %s", a_config_file_name );
        //
        return false;
    }
    //
    if ( ! S_ISREG(v_stat.st_mode) )
    {
        //
        log_error( "CONF: %s is not regular file", a_config_file_name );
        //
        return false;
    }
    // открываем файл
    FILE * v_fp = ::fopen( a_config_file_name, "rb" );
    //
    if ( ! v_fp )
    {
        //
        log_error( "CONF: can't open file %s for read", a_config_file_name );
        //
        return false;
    }
    // место для загрузки файла
    m_config_file = new char[v_stat.st_size + 1];
    //
    if ( ! m_config_file )
    {
        //
        log_error(
                    "CONF: not enough room (%ld) for load config file %s"
                    , v_stat.st_size + 1
                    , a_config_file_name
                    );
        //
        return false;
    }
    // завершающий 0
    m_config_file[v_stat.st_size] = 0;
    // читаем файл
    __off_t v_bytes_readed = ::fread( m_config_file, 1, v_stat.st_size, v_fp );
    //
    ::fclose( v_fp );
    //
    if ( v_bytes_readed != v_stat.st_size )
    {
        //
        log_error(
                    "CONF: can't load config file %s (readed %ld, expected %ld bytes)"
                    , a_config_file_name
                    , v_bytes_readed
                    , v_stat.st_size
                    );
        //
        return false;
    }
    //
    int v_parse_state = ST_OUTSIDE_SECTION;
    int v_last_state = ST_UNKNOWN;
    char * v_current_element = 0;
    CConfigFileObject::SSection v_section;
    CConfigFileObject::SParameter v_param;
    char v_last_symbol = 0;
    char v_string_in_quotes = '\0';
    const char * v_quote_symbol_ptr = 0;
    // разбираем файл
    for ( char * v_ptr = m_config_file; *v_ptr; ++v_ptr )
    {
        //
        if ( '\n' == *v_ptr )
        {
            //
            ++v_line_number;
        }
        //
        switch (v_parse_state)
        {
        // ждём начала секции
        case ST_OUTSIDE_SECTION:
            //
            if ( !isspace( *v_ptr ) )
            {
                //
                if ( ::strchr( COMMENTS_START, *v_ptr ) )
                {
                    //
                    v_last_state = v_parse_state;
                    //
                    v_parse_state = ST_SKIP_COMMENT;
                }
                else
                if ( LEFT_SECTION_BRACKET == *v_ptr )
                {
                    //
                    v_current_element = v_ptr + 1;
                    //
                    v_parse_state = ST_READ_SECTION_NAME;
                }
                else
                {
                    //
                    log_error(
                                "CONF: unexpected symbol '%c' at line %d in file %s"
                                , *v_ptr
                                , v_line_number
                                , a_config_file_name
                                );
                    //
                    cleanup();
                    //
                    return false;
                }
            }
            //
            break;

        // пропускаем комментарий
        case ST_SKIP_COMMENT:
            //
            if ( isspace(*v_ptr) && !isblank(*v_ptr) )
            {
                //
                v_parse_state = v_last_state;
            }
            //
            break;

        // читаем название секции
        case ST_READ_SECTION_NAME:
            //
            if ( isprint(*v_ptr) || is_utf8_part(*v_ptr) )
            {
                //
                if ( RIGHT_INDEX_BRACKET == *v_ptr )
                {
                    // название секции всё
                    *v_ptr = 0;
                    //
                    if ( ::strlen( v_current_element ) > 0 )
                    {
                        //
                        v_section.m_name = v_current_element;
                        //
                        v_current_element = 0;
                        //
                        m_data.push_back( v_section );
                        //
                        v_parse_state = ST_INSIDE_SECTION;
                    }
                    else
                    {
                        //
                        log_error(
                                    "CONF: empty section name at line %d in file %s"
                                    , v_line_number
                                    , a_config_file_name
                                    );
                        //
                        cleanup();
                        //
                        return false;
                    }
                }
            }
            else
            {
                //
                log_error(
                            "CONF: unexpected (nonprintable) symbol within section name at line %d in file %s"
                            , v_line_number
                            , a_config_file_name
                            );
                //
                cleanup();
                //
                return false;
            }
            //
            break;

        // ждём название параметра
        case ST_INSIDE_SECTION:
            //
            if ( !isspace(*v_ptr) )
            {
                //
                if ( ::strchr( COMMENTS_START, *v_ptr ) )
                {
                    //
                    v_last_state = v_parse_state;
                    //
                    v_parse_state = ST_SKIP_COMMENT;
                }
                else
                if ( LEFT_SECTION_BRACKET == *v_ptr )
                {
                    //
                    v_current_element = v_ptr + 1;
                    //
                    v_parse_state = ST_READ_SECTION_NAME;
                }
                else
                if ( !isblank(*v_ptr) )
                {
                    //
                    v_current_element = v_ptr;
                    //
                    v_parse_state = ST_READ_PARAM_NAME;
                }
            }
            //
            break;

        // читаем название параметра
        case ST_READ_PARAM_NAME:
            //
            if ( LEFT_INDEX_BRACKET == *v_ptr || isblank(*v_ptr) || PARAM_START_MARKER == *v_ptr )
            {
                //
                v_last_symbol = *v_ptr;
                // название параметра всё
                *v_ptr = 0;
                //
                if ( ::strlen(v_current_element) )
                {
                    //
                    v_param.m_name = v_current_element;
                    v_param.m_indices.clear();
                    v_param.m_value = 0;
                    //
                    if ( LEFT_INDEX_BRACKET == v_last_symbol )
                    {
                        //
                        v_current_element = v_ptr + 1;
                        //
                        v_parse_state = ST_READ_INDEX;
                    }
                    else
                    if ( PARAM_START_MARKER == v_last_symbol )
                    {
                        //
                        v_current_element = v_ptr + 1;
                        //
                        v_parse_state = ST_WAIT_PARAM_START;
                    }
                    else
                    {
                        //
                        v_parse_state = ST_WAIT_INDEX_START;
                    }
                }
                else
                {
                    //
                    log_error(
                                "CONF: empty param name at line %d in file %s"
                                , v_line_number
                                , a_config_file_name
                                );
                    //
                    cleanup();
                    //
                    return false;
                }
            }
            else
            if ( !(isprint(*v_ptr) || is_utf8_part(*v_ptr)) )
            {
                //
                log_error(
                            "CONF: unexpected (nonprintable) symbol within param name at line %d in file %s"
                            , v_line_number
                            , a_config_file_name
                            );
                //
                cleanup();
                //
                return false;
            }
            //
            break;

        // читаем индекс
        case ST_READ_INDEX:
            //
            if ( isdigit( *v_ptr ) )
            {
                //
                v_parse_state = ST_READ_INDEX_DIGITS;
            }
            else
            if ( !isblank( *v_ptr ) )
            {
                //
                log_error(
                            "CONF: unexpected symbol '%c' within index %u of param %s at line %d in file %s"
                            , *v_ptr
                            , v_param.m_indices.size()
                            , v_param.m_name
                            , v_line_number
                            , a_config_file_name
                            );
                //
                cleanup();
                //
                return false;
            }
            //
            break;

        // читаем цифры
        case ST_READ_INDEX_DIGITS:
            //
            if ( RIGHT_INDEX_BRACKET == *v_ptr )
            {
                // индекс всё
                *v_ptr = 0;
                //
                if ( ::strlen(v_current_element) > 0 )
                {
                    //
                    long v_index = ::strtol( v_current_element, 0, 10 );
                    //
                    if ( (LONG_MIN == v_index || LONG_MAX == v_index || 0 == v_index) && 0 != errno )
                    {
                        //
                        log_error(
                                    "CONF: strtol() error %d, for index %u of param \"%s\" at line %d in file %s"
                                    , errno
                                    , v_param.m_indices.size()
                                    , v_param.m_name
                                    , v_line_number
                                    , a_config_file_name
                                    );
                        //
                        cleanup();
                        //
                        return false;
                    }
                    //
                    v_param.m_indices.push_back( v_index );
                    //
                    v_current_element = 0;
                    //
                    v_parse_state = ST_WAIT_INDEX_START;
                }
                else
                {
                    //
                    log_error(
                                "CONF: empty index %u of param \"%s\" at line %d in file %s"
                                , v_param.m_indices.size()
                                , v_param.m_name
                                , v_line_number
                                , a_config_file_name
                                );
                    //
                    cleanup();
                    //
                    return false;
                }
            }
            else
            {
                //
                if ( !isdigit(*v_ptr) )
                {
                    //
                    log_error(
                                "CONF: symbol '%c' not a number within index %u of param %s at line %d in file %s"
                                , *v_ptr
                                , v_param.m_indices.size()
                                , v_param.m_name
                                , v_line_number
                                , a_config_file_name
                                );
                    //
                    cleanup();
                    //
                    break;
                }
            }
            //
            break;

        // ждём начала индекса или параметра
        case ST_WAIT_INDEX_START:
            //
            if ( LEFT_INDEX_BRACKET == *v_ptr )
            {
                //
                v_current_element = v_ptr + 1;
                //
                v_parse_state = ST_READ_INDEX;
            }
            else
            if ( PARAM_START_MARKER == *v_ptr )
            {
                //
                v_parse_state = ST_WAIT_PARAM_START;
            }
            else
            if ( !isblank(*v_ptr) )
            {
                //
                log_error(
                            "CONF: unexpected symbol '%c' within param %s at line %d in file %s"
                            , *v_ptr
                            , v_param.m_name
                            , v_line_number
                            , a_config_file_name
                            );
                //
                cleanup();
                //
                break;
            }
            //
            break;

        // ждём начала значения параметра
        case ST_WAIT_PARAM_START:
            //
            if ( isprint(*v_ptr) || is_utf8_part(*v_ptr) )
            {
                //
                if ( !isblank(*v_ptr) )
                {
                    //
                    v_quote_symbol_ptr = ::strchr( PARAM_START_STRING, *v_ptr );
                    //
                    if ( v_quote_symbol_ptr )
                    {
                        //
                        v_string_in_quotes = *v_quote_symbol_ptr;
                        //
                        v_current_element = v_ptr + 1;
                    }
                    else
                    {
                        //
                        v_string_in_quotes = '\0';
                        //
                        v_current_element = v_ptr;
                    }
                    //
                    v_parse_state = ST_READ_PARAM_VALUE;
                }
            }
            else
            {
                //
                log_error(
                            "CONF: nonprintable symbol within param %s value at line %d in file %s"
                            , v_param.m_name
                            , v_line_number
                            , a_config_file_name
                            );
                //
                cleanup();
                //
                return false;
            }
            //
            break;

        // читаем значение параметра
        case ST_READ_PARAM_VALUE:
            //
            if ( !(isprint(*v_ptr) || is_utf8_part(*v_ptr))
                 || (isblank(*v_ptr) && !v_string_in_quotes)
                 || (v_string_in_quotes == *v_ptr) )
            {
                // значение параметра всё
                if ( v_string_in_quotes )
                {
                    //
                    if ( v_string_in_quotes != *v_ptr )
                    {
                        //
                        log_error(
                                    "CONF: expected trailing quote(s) %c within param %s value al line %d in file %s"
                                    , v_string_in_quotes
                                    , v_param.m_name
                                    , v_line_number
                                    , a_config_file_name
                                    );
                        //
                        cleanup();
                        //
                        return false;
                    }
                }
                //
                *v_ptr = 0;
                //
                if ( v_ptr < v_current_element )
                {
                    //
                    log_error(
                                "CONF: can't read param %s value at line %d in file %s"
                                , v_param.m_name
                                , v_line_number
                                , a_config_file_name
                                );
                    //
                    cleanup();
                    //
                    return false;
                }
                //
                v_param.m_value = v_current_element;
                //
                m_data[m_data.size() - 1].m_params.push_back(v_param);
                //
                v_param.m_name = 0;
                v_param.m_indices.clear();
                v_param.m_value = 0;
                //
                v_parse_state = ST_INSIDE_SECTION;
            }
            //
            break;
        }
    }
    // сортировка секций
    std::sort( m_data.begin(), m_data.end() );
    // сортировка параметров в секциях
    for ( CConfigFileObject::SSection & i: m_data )
    {
        //
        std::sort( i.m_params.begin(), i.m_params.end() );
    }
    //
    return true;
}


bool compare_for_param_names( const CConfigFileObject::SParameter & a_1, const CConfigFileObject::SParameter & a_2 )
{
    //
    return ::strcmp( a_1.m_name, a_2.m_name ) < 0;
}

//
bool CConfigFileObject::get_param_entries(CConfigFileObject::param_entries_t & a_dst, const char *a_section, const char *a_param) const
{
    CConfigFileObject::SSection v_section;

    //
    v_section.m_name = a_section;
    //
    std::pair<sections_t::const_iterator, sections_t::const_iterator> v_section_it
            = std::equal_range(
                m_data.cbegin()
                , m_data.cend()
                , v_section
                );
    //
    if ( v_section_it.first != v_section_it.second )
    {
        CConfigFileObject::SParameter v_param;

        //
        v_param.m_name = a_param;
        //
        a_dst = std::equal_range(
                    v_section_it.first->m_params.cbegin()
                    , v_section_it.first->m_params.cend()
                    , v_param
                    , compare_for_param_names
                    );
        //
        return true;
    }
    else
    {
        //
        log_error(
                    "CONF[%s:%d]: section %s not found"
                    , __FILE__
                    , __LINE__
                    , a_section
                    );
        //
        return false;
    }
}
