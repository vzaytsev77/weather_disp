#include <string>
#include <curl/curl.h>
#include <sys/time.h>
#include "class_parse_json.h"
#include "cconfigfileobject.h"



#define CFG_OWM_SECTION                 "OpenWeatherMap"
#define MAX_WEATHER_REPLY_SIZE          32768


struct weatherinfo
{
    // дата/время
    struct timeval m_ts;
    // температура, градусы цельсия
    double m_temp;
    // давление, гПа (гектопаскаль)
    double m_pressure;
    // влажность, %
    int m_humidity;
    // скорость ветра, м/с
    double m_windspeed;
    // название иконки погодных условий
    std::string m_icon;
    //
    weatherinfo()
        : m_temp(0.0)
        , m_pressure(0.0)
        , m_humidity(0)
        , m_windspeed(0.0)
    {
        //
        ::bzero( &m_ts, sizeof(m_ts) );
    }        
    //
    bool fill_from_json( const CParseJson & a_pj );
    //
    bool fill_from_json( const Json::Value & a_obj );
};



struct openweathermap
{
    // ctor
    openweathermap();
    // dtor
    ~openweathermap();
    //
    void startup( const CConfigFileObject & a_cfg );
    //
    const char * get_icon( const char * a_icon_name );
    //
    bool get_weather();
    //
    bool get_forecast();
    
    // настроечные параметры
    // базовый URL для запроса
    const char * m_req_url;
    // ключ API OpenWeatherMap
    const char * m_api_key;
    // идентификатор места
    const char * m_place_id;
    // команда запроса погоды на сейчас
    const char * m_req_weather;
    // команда запроса прогноза на 5 дней
    const char * m_req_forecast;
    // шаблон запроса
    const char * m_req_template;
    // шаблон URL запроса иконки (для кода погодных условий)
    const char * m_req_icon;
    // директорий для кэша иконок
    const char * m_icon_cache_dir;
    // таймаут на подключение, секунды
    int m_connect_timeout;
    // таймаут на выполнение запроса
    int m_request_timeout;
    //
    std::string m_last_icon_path;
    //
    std::string m_received_json;
    //
    CParseJson m_parsed_weather;
    CParseJson m_parsed_forecast;
};
