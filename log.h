#pragma once

#include <stdio.h>

#ifdef __GNUC__
#include   <syslog.h>
#endif //__GNUC__

// ухищрение чтобы представить __LINE__ строкой
#define STRINGIZE(x) STRINGIZE2(x)
#define STRINGIZE2(x) #x
#define LINE_STRING STRINGIZE(__LINE__)

//
class CLogClassHelper
{
public:
    static int get_syslog_class();
    static void set_syslog_class( int );

private:
    static int m_syslog_class;
};

#define    LOG_CLASS (CLogClassHelper::get_syslog_class())
//
#define    log_start(name)  ::openlog(name, LOG_NDELAY, LOG_CLASS)
#define    log_stop()       ::closelog()

#define    log_debug(...)   ::syslog(LOG_CLASS | LOG_DEBUG,  "[DEB] " __VA_ARGS__)
#define    log_info(...)    ::syslog(LOG_CLASS | LOG_INFO,   "[INF] " __VA_ARGS__)
#define    log_warning(...) ::syslog(LOG_CLASS | LOG_WARNING,"[WAR] " __VA_ARGS__)
#define    log_error(...)   ::syslog(LOG_CLASS | LOG_ERR,    "[ERR] " __VA_ARGS__)

//#define    log_debug(...)   {::printf( "[DEB] " __VA_ARGS__);::printf("\n");}
//#define    log_info(...)    {::printf( "[INF] " __VA_ARGS__);::printf("\n");}
//#define    log_warning(...) {::printf( "[WAR] " __VA_ARGS__);::printf("\n");}
//#define    log_error(...)   {::printf( "[ERR] " __VA_ARGS__);::printf("\n");}

// записать в лог буфер в шестнадцатиричном форматированном виде
void logBufferInHex(const unsigned char * src, int len,
        unsigned int a_log_level = 30);

/** Уровень болтливости debug-а. */
extern unsigned int     g_log_debug_level;

/** устанавливает текущий уровень болтливости debug-а.
 *  0  - no debug
 *  2  - сообщения о всех catch-ах
 *  5  - сообщения о старте "обычной" функции
 *  10 - обычные сообщения
 *  20 - сообщения о старте частовызываемой функции */

namespace lottery
{
    namespace base
    {
        namespace log
        {
            enum SCALE_DEBUG_LEVELS {
                 NO_DEBUG = 0 // выводятся только штатные сообщения
                ,WARNING = 2 // сообщения о всех catch-ах и прочих не совсем корректных операциях
                ,INFO = 5 // сообщения о завершении операций ( например, отправка сообщения, запись в БД )
                ,LOW_DEBUG = 10 // сообщения о старте основных функций
                ,MID_DEBUG = 20 // сообщения о старте основной функции , значения аргументов и  результаты
                ,FULL_DEBUG = 40 // сообщения о выполненных шагах , исходные данные для шага цикла и ре-ты
                ,VERY_FULL_DEBUG = 60 // сообщения о старте часто-вызываемой функции
                ,PARANOIC_DEBUG = 80 // плюс к предыдущему значения входных и выходных параметров
            };
        }
    }
}

/** Выдает сообщение только,
 *  если общий уровень болтливости не ниже "level" */
#define log_debug_n(level,...) do { \
        if ( ::g_log_debug_level>=level ) \
            log_debug(__VA_ARGS__); \
    } while (0)

