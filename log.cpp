#include "log.h"

unsigned int g_log_debug_level = 2;

int CLogClassHelper::m_syslog_class = LOG_LOCAL7;

int CLogClassHelper::get_syslog_class()
{
    //
    return m_syslog_class;
}

void CLogClassHelper::set_syslog_class( int a_syslog_class )
{
    //
    if (    LOG_LOCAL0 == a_syslog_class
         || LOG_LOCAL1 == a_syslog_class
         || LOG_LOCAL2 == a_syslog_class
         || LOG_LOCAL3 == a_syslog_class
         || LOG_LOCAL4 == a_syslog_class
         || LOG_LOCAL5 == a_syslog_class
         || LOG_LOCAL6 == a_syslog_class
         || LOG_LOCAL7 == a_syslog_class )
    {
        //
        m_syslog_class = a_syslog_class;
    }
}


