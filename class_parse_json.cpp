#include "log.h"
#include "class_parse_json.h"
#include <errno.h>


//
CParseJson::SParseJsonDescMeta::SParseJsonDescMeta()
    : m_field_names_sorted(false)
{
    int v_rc = ::pthread_mutex_init( &m_mutex, 0 );
    //
    if ( v_rc )
    {
        //
        log_error(
                    "%s [%d] pthread_mutex_init() failed with error %d"
                    , __FILE__
                    , __LINE__
                    , v_rc
                    );
        //
        ::exit( EXIT_FAILURE );
    }
}


//
CParseJson::SParseJsonDescMeta::~SParseJsonDescMeta()
{
    //
    ::pthread_mutex_destroy( &m_mutex );
}


// -----------
static int compare_two_SParseJsonDesc_by_m_name( const void * a_1, const void * a_2 )
{
    return ::strcmp(
          ((const CParseJson::SParseJsonDesc *)a_1)->m_name
        , ((const CParseJson::SParseJsonDesc *)a_2)->m_name
        );
}



bool CParseJson::get_json_values( const char * a_json_text )
{
    //
    m_desc_meta->m_json_root.clear();
    //
    if ( m_json_reader.parse( a_json_text, m_desc_meta->m_json_root, false ) )
    {
        //
        return get_json_values( m_desc_meta->m_json_root );
    }
    else
    {
        //
        log_error(
                    "%s [%d] can't parse json '%s'"
                    , __FILE__
                    , __LINE__
                    , a_json_text
                    );
        //
        return false;
    }
}


bool CParseJson::get_json_values( Json::Value & a_json )
{
    Json::Value * v_ptr;
    bool v_result = true;

    // по всем определениям полей
    for ( size_t u = 0; v_result && u < m_desc_count; ++u )
    {
        //
        v_ptr = & a_json[m_json_desc[u].m_name];
        //
        switch ( m_json_desc[u].m_value_type )
        {
        case Json::stringValue:
            //
            if ( v_ptr->isNull() )
            {
                // поле отсутствует, может есть значение по-умолчанию
                if ( m_json_desc[u].m_default_value )
                {
                    // да будет так
                    *v_ptr = Json::Value(m_json_desc[u].m_default_value);
                }
                else
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" (string) does not exists, and no default value provided"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            else
            {
                if ( ! v_ptr->isString() )
                {
                        //
                        v_result = false;
                        //
                        log_error(
                                    "%s [%d] field \"%s\" type mismatch: expected string"
                                    , __FILE__
                                    , __LINE__
                                    , m_json_desc[u].m_name
                                    );
                }
            }
            break;

        case Json::intValue:
            //
            if ( v_ptr->isNull() )
            {
                // поле отсутствует, может есть значение по-умолчанию
                if ( m_json_desc[u].m_default_value )
                {
                    // да будет так
                    long v_int_value = ::strtol( m_json_desc[u].m_default_value, 0, 10 );
                    //
                    if ((ERANGE == errno && (LONG_MAX == v_int_value || LONG_MIN == v_int_value))
                            || (0 != errno && 0 == v_int_value))
                    {
                        //
                        v_result = false;
                        //
                        log_error(
                                    "%s [%d] default value \"%s\" for field \"%s\" (integral) seems to be not a number"
                                    , __FILE__
                                    , __LINE__
                                    , m_json_desc[u].m_default_value
                                    , m_json_desc[u].m_name
                                    );
                    }
                    else
                    {
                        //
                        *v_ptr = Json::Value(Json::Int64(v_int_value));
                    }
                }
                else
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" (integral) does not exists, and no default value provided"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            else
            {
                if ( ! v_ptr->isIntegral() )
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" type mismatch: expected integral"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            break;

        case Json::arrayValue:
            //
            if ( v_ptr->isNull() )
            {
                // поле отсутствует, может есть значение по-умолчанию
                if ( m_json_desc[u].m_default_value )
                {
                    // да будет так
                    Json::Value v_default_root;
                    //
                    if ( m_json_reader.parse( m_json_desc[u].m_default_value, v_default_root, false ) )
                    {
                        //
                        if ( v_default_root[PARSE_JSON_VALUE_DEFAULT_NAME].isArray() )
                        {
                            //
                            *v_ptr = v_default_root[PARSE_JSON_VALUE_DEFAULT_NAME];
                        }
                        else
                        {
                            //
                            v_result = false;
                            //
                            log_error(
                                        "%s [%d] for field \"%s\" (array) default value \"%s\" not contains \"%s\""
                                        , __FILE__
                                        , __LINE__
                                        , m_json_desc[u].m_name
                                        , m_json_desc[u].m_default_value
                                        , PARSE_JSON_VALUE_DEFAULT_NAME
                                        );
                        }
                    }
                    else
                    {
                        //
                        v_result = false;
                        //
                        log_error(
                                    "%s [%d] for field \"%s\" (array) default value \"%s\" is invalid"
                                    , __FILE__
                                    , __LINE__
                                    , m_json_desc[u].m_name
                                    , m_json_desc[u].m_default_value
                                    );
                    }
                }
                else
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" (array) does not exists, and no default value provided"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            else
            {
                if ( ! v_ptr->isArray() )
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" type mismatch: expected array"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            //
            break;

        case Json::objectValue:
            //
            if ( v_ptr->isNull() )
            {
                // поле отсутствует, может есть значение по-умолчанию
                if ( m_json_desc[u].m_default_value )
                {
                    // да будет так
                    Json::Value v_default_root;
                    //
                    if ( m_json_reader.parse( m_json_desc[u].m_default_value, v_default_root, false ) )
                    {
                        //
                        if ( v_default_root[PARSE_JSON_VALUE_DEFAULT_NAME].isObject() )
                        {
                            //
                            *v_ptr = v_default_root[PARSE_JSON_VALUE_DEFAULT_NAME];
                        }
                        else
                        {
                            //
                            v_result = false;
                            //
                            log_error(
                                        "%s [%d] for field \"%s\" (object) default value \"%s\" not contains \"%s\""
                                        , __FILE__
                                        , __LINE__
                                        , m_json_desc[u].m_name
                                        , m_json_desc[u].m_default_value
                                        , PARSE_JSON_VALUE_DEFAULT_NAME
                                        );
                        }
                    }
                    else
                    {
                        //
                        v_result = false;
                        //
                        log_error(
                                    "%s [%d] for field \"%s\" (object) default value \"%s\" is invalid"
                                    , __FILE__
                                    , __LINE__
                                    , m_json_desc[u].m_name
                                    , m_json_desc[u].m_default_value
                                    );
                    }
                }
                else
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" (object) does not exists, and no default value provided"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            else
            {
                if ( ! v_ptr->isObject() )
                {
                    //
                    v_result = false;
                    //
                    log_error(
                                "%s [%d] field \"%s\" type mismatch: expected object"
                                , __FILE__
                                , __LINE__
                                , m_json_desc[u].m_name
                                );
                }
            }
            //
            break;

        default:
            // неопознанный тип
            log_error(
                        "%s [%d] unexpected type in decription of field \"%s\""
                        , __FILE__
                        , __LINE__
                        , m_json_desc[u].m_name
                        );
            //
            v_result = false;
            break;
        }
        // set up pointer
        if ( v_result )
        {
            //
            m_json_desc[u].m_value = v_ptr;
        }
    }
    //
    return v_result;
}


//
const Json::Value * CParseJson::operator [] ( const char * a_field_name ) const
{
    SParseJsonDesc v_key;

    //
    v_key.m_name = a_field_name;
    //
    SParseJsonDesc * v_ptr = (SParseJsonDesc * )
            ::bsearch(
                &v_key
                , m_json_desc
                , m_desc_count
                , sizeof(*m_json_desc)
                , compare_two_SParseJsonDesc_by_m_name
                );
    //
    if ( ! v_ptr )
    {
        //
        log_error(
                    "%s [%d] unexpected field name \"%s\""
                    , __FILE__
                    , __LINE__
                    , a_field_name
                    );
        //
        ::exit( EXIT_FAILURE );
    }
    //
    if ( ! v_ptr->m_value )
    {
        //
        log_error(
                    "%s [%d] field \"%s\" not parsed properly"
                    , __FILE__
                    , __LINE__
                    , a_field_name
                    );
        //
        ::exit( EXIT_FAILURE );
    }
    //
    return v_ptr->m_value;
}


//
CParseJson::CParseJson(
        SParseJsonDesc *a_json_desc
        , size_t a_desc_count
        , SParseJsonDescMeta * a_desc_meta
        )
    : m_json_desc(a_json_desc)
    , m_desc_count(a_desc_count)
    , m_desc_meta(a_desc_meta)
{
    //
    ::pthread_mutex_lock( & a_desc_meta->m_mutex );
    // сортируем по названию поля
    if ( ! a_desc_meta->m_field_names_sorted )
    {
        //
        ::qsort(
                a_json_desc
                , a_desc_count
                , sizeof(*a_json_desc)
                , compare_two_SParseJsonDesc_by_m_name
                );
        //
        a_desc_meta->m_field_names_sorted = true;
    }
}

//
CParseJson::~CParseJson()
{
    //
    ::pthread_mutex_unlock( & m_desc_meta->m_mutex );
}
