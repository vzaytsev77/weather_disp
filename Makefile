CXX=g++
CC=gcc
# CXXFLAGS=-std=c++14 -O2 -Wall -Wextra
# CCFLAGS=-O2 -Wall -Wextra
# LDFLAGS=-s
CCFLAGS=-O0 -g -Wall -Wextra
CXXFLAGS=-std=c++14 -O0 -g -Wall -Wextra
LDFLAGS=-g
LINK=g++
TARGET=weather_disp
LIBS=-lSDL_ttf -lfreetype -lSDL_image -lSDL -lcurl -ljsoncpp -lpng -pthread -lz

all: ${TARGET}

${TARGET}: $(patsubst %.cpp,%.o,$(wildcard *.cpp)) $(patsubst %.c,%.o,$(wildcard *.c))
	${PLATFORM}${LINK} $^ ${LDFLAGS} -L${TOOLS}/usr/local/lib ${LIBS} -o ${TARGET}

%.o: %.cpp
	${PLATFORM}${CXX} $< -I${TOOLS}/usr/local/include -c -MMD ${CXXFLAGS}

%.o: %.c
	${PLATFORM}${CC} $< -I${TOOLS}/usr/local/include -c -MMD ${CCFLAGS}

include $(wildcard *.d)


clean:
	rm -f *.o *.d ${TARGET}
