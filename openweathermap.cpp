//
#include "openweathermap.h"
#include "log.h"
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>


#define ARRAY_LEN(a) (sizeof(a)/sizeof(a[0]))

CParseJson::SParseJsonDescMeta g_parse_weather_meta;
CParseJson::SParseJsonDesc g_parse_weather[] =
{
    {0, Json::arrayValue , "weather", "[]"}
  , {0, Json::intValue   , "dt"     , "0" }
  , {0, Json::objectValue, "main"   , "{}"}
  , {0, Json::objectValue, "wind"   , "{}"}
  , {0, Json::objectValue, "clouds" , "{}"}
  , {0, Json::objectValue, "sys"    , "{}"}
  , {0, Json::stringValue, "name"   , "Somewhere"}
};


CParseJson::SParseJsonDescMeta g_parse_forecast_meta;
CParseJson::SParseJsonDesc g_parse_forecast[] =
{
    {0, Json::intValue   , "cnt" , "0" }
  , {0, Json::arrayValue , "list", "[]"}
};


//
openweathermap::openweathermap()
    : m_parsed_weather( g_parse_weather, ARRAY_LEN(g_parse_weather), &g_parse_weather_meta )
    , m_parsed_forecast( g_parse_forecast, ARRAY_LEN(g_parse_forecast), &g_parse_forecast_meta )
{
    //
    CURLcode v_result = curl_global_init( CURL_GLOBAL_ALL );
    //
    if ( CURLE_OK != v_result )
    {
        //
        log_error(
            "curl_global_init() returns error %d"
            , v_result
            );
        //
        ::exit( EXIT_FAILURE );
    }
    //
    log_info( "openweathermap object created" );
}


//
openweathermap::~openweathermap()
{
    //
    curl_global_cleanup();
    //
    log_info( "openweathermap object destroyed" );
}


//
void openweathermap::startup( const CConfigFileObject & a_cfg )
{
    //
    if ( !a_cfg.get_string( m_req_url, CFG_OWM_SECTION, "url" )
      || !a_cfg.get_string( m_api_key, CFG_OWM_SECTION, "api_key" )
      || !a_cfg.get_string( m_place_id, CFG_OWM_SECTION, "place_id" )
      || !a_cfg.get_string( m_req_weather, CFG_OWM_SECTION, "req_weather" )
      || !a_cfg.get_string( m_req_forecast, CFG_OWM_SECTION, "req_forecast" )
      || !a_cfg.get_string( m_req_template, CFG_OWM_SECTION, "req_template" )
      || !a_cfg.get_string( m_req_icon, CFG_OWM_SECTION, "req_icon" )
      || !a_cfg.get_string( m_icon_cache_dir, CFG_OWM_SECTION, "icon_cache_dir" )
      || !a_cfg.get_long  ( m_connect_timeout, CFG_OWM_SECTION, "connect_timeout" )
      || !a_cfg.get_long  ( m_request_timeout, CFG_OWM_SECTION, "request_timeout" ) )
    {
        //
        log_error(
            "incorrect config file"
            );
        //
        ::exit( EXIT_FAILURE );
    }
    // директорий для кэша иконок
    if ( 0 != ::mkdir( m_icon_cache_dir, 0777 ) )
    {
        //
        if ( EEXIST != errno )
        {
            //
            log_error(
                "can't create directory \"./icon_cache\""
                );
            //
            ::exit( EXIT_FAILURE );
        }
    }
}


// 
const char * openweathermap::get_icon( const char * a_icon_name )
{
    char v_line[2048];
    
    // пробуем найти файл
    if ( sizeof(v_line) <= (size_t)::snprintf(
              v_line
            , sizeof(v_line)
            , "%s/%s.png"
            , m_icon_cache_dir
            , a_icon_name
            ) )
    {
        //
        log_error(
            "%s [%d] not enough room for icon path"
            , __FILE__
            , __LINE__
            );
        //
        return 0;
    }
    //
    m_last_icon_path = v_line;
    //
    struct stat v_stat;
    //
    ::bzero( &v_stat, sizeof(v_stat) );
    //
    if ( 0 == ::stat( v_line, &v_stat ) )
    {
        //
        if ( S_ISREG(v_stat.st_mode) || S_ISLNK(v_stat.st_mode) )
        {
            //
            if ( 0 != v_stat.st_size )
            {
                //
                return m_last_icon_path.c_str();
            }
        }
        else
        {
            //
            log_error(
                "path %s not a regular file nor a symbolic link"
                , v_line
                );
            //
            return 0;
        }
    }
    else
    {
        //
        if ( ENOENT != errno )
        {
            //
            log_error(
                "%s [%d] stat(%s) error, errno = %d"
                , __FILE__
                , __LINE__
                , v_line
                , errno
                );
            //
            return 0;
        } 
    }
    //
    FILE * v_fp_icon = ::fopen( v_line, "wb" );
    //
    if ( ! v_fp_icon )
    {
            //
            log_error(
                "%s [%d] fopen(%s, 'wb') error, errno = %d"
                , __FILE__
                , __LINE__
                , v_line
                , errno
                );
            //
            return 0;
    }
    // дёргаем картинку из инета
    if ( sizeof(v_line) <= (size_t)::snprintf(
                    v_line
                    , sizeof(v_line)
                    , m_req_icon
                    , a_icon_name
                    ) )
    {
        //
        log_error(
            "%s [%d] not enough room for icon request"
            , __FILE__
            , __LINE__
            );
        //
        ::fclose( v_fp_icon );
        //
        return 0;
    }
    //
    CURL * v_curl = ::curl_easy_init();
    //
    if ( ! v_curl )
    {
        //
        log_error(
            "%s [%d] curl_easy_init() error"
            , __FILE__
            , __LINE__
            );
        //
        ::fclose( v_fp_icon );
        //
        return 0;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_WRITEDATA, (void *)v_fp_icon ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_WRITEDATA) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        ::fclose( v_fp_icon );
        //
        return 0;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_URL, v_line ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_URL) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        ::fclose( v_fp_icon );
        //
        return 0;
    }
    //
    if ( CURLE_OK != ::curl_easy_perform( v_curl ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_perform() error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        ::fclose( v_fp_icon );
        //
        return 0;
    }
    else
    {
        //
        ::curl_easy_cleanup( v_curl );
        ::fclose( v_fp_icon );
        //
        return m_last_icon_path.c_str();
    }
}


//
size_t write_callback(char * a_ptr, size_t a_size, size_t a_nmemb, void * a_userdata)
{
    openweathermap * v_obj = (openweathermap *)a_userdata;
    size_t v_to_copy = a_size * a_nmemb;
    
    //
    if ( (v_obj->m_received_json.size() + v_to_copy) > MAX_WEATHER_REPLY_SIZE )
    {
        //
        v_to_copy = (v_obj->m_received_json.size() + v_to_copy) - MAX_WEATHER_REPLY_SIZE;
    }
    //
    v_obj->m_received_json.append( a_ptr, v_to_copy );
    //
    return v_to_copy;
}



//
bool openweathermap::get_weather()
{
    char v_line[2048];

    // сочиняем URL
    if ( sizeof(v_line) <= (size_t)::snprintf(  v_line
                                                , sizeof(v_line)
                                                , m_req_template
                                                , m_req_url
                                                , m_req_weather
                                                , m_place_id
                                                , m_api_key ) )
    {
        //
        log_error(
            "%s [%d] not enough room for request URL"
            , __FILE__
            , __LINE__
            );
        //
        return false;
    }
    //
    m_received_json.clear();
    // закидываем запрос
    CURL * v_curl = ::curl_easy_init();
    //
    if ( ! v_curl )
    {
        //
        log_error(
            "%s [%d] curl_easy_init() error"
            , __FILE__
            , __LINE__
            );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_WRITEFUNCTION, write_callback ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_WRITEFUNCTION) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_WRITEDATA, (void *)this ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_WRITEDATA) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_URL, v_line ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_URL) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_perform( v_curl ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_perform() error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    else
    {
        //
        //log_info( "%s", m_received_json.c_str() );
        //
        ::curl_easy_cleanup( v_curl );
    }
    //
    if ( m_parsed_weather.get_json_values( m_received_json.c_str() ) )
    {
        //
        return true;
    }
    else
    {
        //
        log_error(
              "%s [%d] weather json not parsed: %s"
            , __FILE__
            , __LINE__
            , m_received_json.c_str()
            );
        //
        return false;
    }
}


//
bool openweathermap::get_forecast()
{
    char v_line[2048];

    // сочиняем URL
    if ( sizeof(v_line) <= (size_t)::snprintf(  v_line
                                                , sizeof(v_line)
                                                , m_req_template
                                                , m_req_url
                                                , m_req_forecast
                                                , m_place_id
                                                , m_api_key ) )
    {
        //
        log_error(
            "%s [%d] not enough room for request URL"
            , __FILE__
            , __LINE__
            );
        //
        return false;
    }
    //
    m_received_json.clear();
    // закидываем запрос
    CURL * v_curl = ::curl_easy_init();
    //
    if ( ! v_curl )
    {
        //
        log_error(
            "%s [%d] curl_easy_init() error"
            , __FILE__
            , __LINE__
            );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_WRITEFUNCTION, write_callback ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_WRITEFUNCTION) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_WRITEDATA, (void *)this ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_WRITEDATA) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_setopt( v_curl, CURLOPT_URL, v_line ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_setopt(CURLOPT_URL) error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    //
    if ( CURLE_OK != ::curl_easy_perform( v_curl ) )
    {
        //
        log_error(
            "%s [%d] curl_easy_perform() error"
            , __FILE__
            , __LINE__
            );
        //
        ::curl_easy_cleanup( v_curl );
        //
        return false;
    }
    else
    {
        //
        //log_info( "%s", m_received_json.c_str() );
        //
        ::curl_easy_cleanup( v_curl );
    }
    //
    if ( m_parsed_forecast.get_json_values( m_received_json.c_str() ) )
    {
        //
        return true;
    }
    else
    {
        //
        log_error(
              "%s [%d] forecast json not parsed: %s"
            , __FILE__
            , __LINE__
            , m_received_json.c_str()
            );
        //
        return false;
    }
}



//
bool weatherinfo::fill_from_json( const CParseJson & a_pj )
{
    try
    {
        // дата/время UTC
        m_ts.tv_sec = a_pj["dt"]->asUInt();
        m_ts.tv_usec = 0;
        //
        const Json::Value * v_vptr = a_pj["main"];
        // температура
        m_temp = (*v_vptr)["temp"].asDouble();
        // давление
        m_pressure = (*v_vptr)["pressure"].asDouble();
        // влажность относительная
        m_humidity = (*v_vptr)["humidity"].asInt();
        //
        v_vptr = a_pj["wind"];
        // скорость ветра
        m_windspeed = (*v_vptr)["speed"].asDouble();
        //
        v_vptr = a_pj["weather"];
        // иконка погодных условий
        m_icon = (*v_vptr)[(Json::ArrayIndex)0]["icon"].asCString();
        //
        return true;
    }
    catch (Json::Exception & e)
    {
        //
        log_error(
            "%s"
            , e.what()
            );
    }
    //
    return false;
}


//
bool weatherinfo::fill_from_json( const Json::Value & a_obj )
{
    try
    {
        // дата/время UTC
        m_ts.tv_sec = a_obj["dt"].asUInt();
        m_ts.tv_usec = 0;
        //
        const Json::Value & v_main = a_obj["main"];
        // температура
        m_temp = v_main["temp"].asDouble();
        // давление
        m_pressure = v_main["pressure"].asDouble();
        // влажность относительная
        m_humidity = v_main["humidity"].asInt();
        //
        const Json::Value & v_wind = a_obj["wind"];
        // скорость ветра
        m_windspeed = v_wind["speed"].asDouble();
        //
        const Json::Value & v_weather = a_obj["weather"];
        // иконка погодных условий
        m_icon = v_weather[(Json::ArrayIndex)0]["icon"].asCString();
        //
        return true;
    }
    catch (Json::Exception & e)
    {
        //
        log_error(
            "%s"
            , e.what()
            );
    }
    //
    return false;
}
