#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>
#include "openweathermap.h"
#include "log.h"


#define WD_SCREEN_WIDTH     320
#define WD_SCREEN_HEIGHT    240
#define WD_SCREEN_BPP       16

#define FNT_SIZE_FORECAST   14


#define FNT "/usr/share/fonts/truetype/freefont/FreeSansBold.ttf"
#define MM_HG               0.750064


//
volatile bool g_stop_flag = false;

//
void sig_handler( int a_signal, siginfo_t * a_siginfo, void * a_ctx )
{
    //
    log_info(
        "signal: %d, si_pid = %d, uctx = %08lX"
        , a_signal
        , a_siginfo->si_pid
        , (unsigned long)a_ctx
        );
    //
    g_stop_flag = true;
}




openweathermap g_owm;
CConfigFileObject g_cfg;


SDL_Rect print_ttf(SDL_Surface *sDest, const char* message, const char* font, int size, SDL_Color color, SDL_Rect dest);

//
int main( int argc, char ** argv )
{
    //
    if ( argc != 2 )
    {
        //
        ::printf( "%s <config_file_path>\n", argv[0] );
        //
        return EXIT_FAILURE;
    }

    //
    struct sigaction v_sa;
    ::bzero( &v_sa, sizeof v_sa);
    //
    v_sa.sa_sigaction = sig_handler;
    v_sa.sa_flags = SA_SIGINFO;
    __sigfillset(&v_sa.sa_mask);
    //
    if ( -1 == ::sigaction( SIGTERM, &v_sa, 0 ) )
    {
        //
        log_error( "sigaction(SIGTERM) failed with errno = %d", errno );
        //
        return EXIT_FAILURE;
    }
    //
    if ( -1 == ::sigaction( SIGINT, &v_sa, 0 ) )
    {
        //
        log_error( "sigaction(SIGINT) failed with errno = %d", errno );
        //
        return EXIT_FAILURE;
    }
    
    //
    if ( ! g_cfg.parse_file( argv[1] ) )
    {
        //
        log_error( "error parsing config file \"%s\"", argv[1] );
        //
        return EXIT_FAILURE;
    }
    //
    g_owm.startup( g_cfg );
    //
    if ( SDL_Init(SDL_INIT_VIDEO) < 0 )
    {
        //
        ::printf("Unable to init SDL: %s\n", SDL_GetError());
        //
        return EXIT_FAILURE;
    }
    //
    ::atexit(SDL_Quit);
    //
    TTF_Init();
    ::atexit(TTF_Quit);
    //
    SDL_ShowCursor( SDL_DISABLE );
    //
    const char * v_foreground = 0;
    SDL_Surface * v_bg = 0;
    //
    if (  g_cfg.get_string( v_foreground, "misc", "foreground" ) )
    {
        //
        v_bg = IMG_Load( v_foreground );
    }
    //
    const char * v_icon_name = 0;
    //
    TTF_Font *v_fnt = TTF_OpenFont(FNT, 36);
    //
    if ( ! v_fnt )
    {
        //
        log_error(
            "TTF_OpenFont(\"" FNT "\", 36) returns null"
            );
        //
        ::exit( EXIT_FAILURE );
    }
    TTF_CloseFont( v_fnt );
    //
    SDL_Surface * v_scr = 0;
    SDL_Surface * v_icon = 0;
    //
    v_scr = SDL_SetVideoMode(
          WD_SCREEN_WIDTH
        , WD_SCREEN_HEIGHT
        , 0
        , SDL_SWSURFACE | SDL_FULLSCREEN
        );
    //
    if ( ! v_scr )
    {
        //
        ::printf("Error create surface: %s\n", SDL_GetError());
        //
        return EXIT_FAILURE;
    }
    //
    while ( !g_stop_flag )
    {
        SDL_Rect v_rc, v_drc;
        char v_ln[128];
        struct tm v_tm;

        //
        if ( g_owm.get_weather() )
        {
            //
            weatherinfo v_wi;
            //
            if ( v_wi.fill_from_json( g_owm.m_parsed_weather ) )
            {
                //
                SDL_Color v_clr;
                v_clr.r = 173;
                v_clr.g = 50;
                v_clr.b = 55;
                //
                v_rc.x = 0;
                v_rc.y = 0;
                //
                if ( v_bg )
                {
                    //
                    SDL_BlitSurface( v_bg, 0, v_scr, &v_rc );
                }
                //
                v_icon_name = (*g_owm.m_parsed_weather["weather"])[(Json::ArrayIndex)0]["icon"].asCString();
                //
                const char * v_icon_path = g_owm.get_icon( v_icon_name );
                //
                if ( v_icon_path )
                {
                    //
                    v_icon = IMG_Load( v_icon_path );
                    //
                    SDL_BlitSurface( v_icon, 0, v_scr, &v_rc );
                }
                //
                ::bzero( &v_tm, sizeof(v_tm) );
                //
                ::sprintf( v_ln, "%.2f°C", (*g_owm.m_parsed_weather["main"])["temp"].asDouble() );
                v_rc.x = 54;
                v_rc.y = 0;
                v_drc = print_ttf(v_scr, v_ln, FNT, 36, v_clr, v_rc );
                //
                ::sprintf(
                    v_ln
                    , "%d мм, %d %%, %d м/с"
                    , (int)((v_wi.m_pressure * MM_HG) + 0.5)
                    , v_wi.m_humidity
                    , (int)(v_wi.m_windspeed + 0.5)
                    );
                v_rc.x = 4;
                v_rc.y = 52;
                v_clr.b = 239;
                v_clr.r = 57;
                print_ttf(v_scr, v_ln, FNT, 24, v_clr, v_rc );
                //
                ::localtime_r( &v_wi.m_ts.tv_sec, &v_tm );
                ::strftime( v_ln, sizeof(v_ln), "%T", &v_tm );
                v_rc.x = 54 + v_drc.w + 8;
                v_rc.y = 0;
                v_clr.g = 79;
                v_clr.b = 18;
                v_clr.r = 14;
                v_drc = print_ttf(v_scr, v_ln, FNT, 24, v_clr, v_rc );
                //
                ::strftime( v_ln, sizeof(v_ln), "%F", &v_tm );
                v_rc.y = v_drc.h + 2;
                print_ttf(v_scr, v_ln, FNT, 24, v_clr, v_rc );
                //
                if ( g_owm.get_forecast() )
                {
                    struct timeval v_tv;
                    const Json::Value * v_arr = g_owm.m_parsed_forecast["list"];
                    Json::ArrayIndex k = 0;
                    Json::ArrayIndex v_step = 1;
                    SDL_Rect v_rc2;

                    //
                    v_clr.g = 205;
                    v_clr.b = 215;
                    v_clr.r = 210;
                    //
                    v_rc.x = 2;
                    v_rc.y = 100;
                    //
                    ::gettimeofday( &v_tv, 0 );
                    //
                    for ( Json::ArrayIndex i = 0; i < v_arr->size() && k < 6; i += v_step )
                    {
                        //
                        if ( v_wi.fill_from_json( (*v_arr)[i] ) )
                        {
                            //
                            if ( v_wi.m_ts.tv_sec > v_tv.tv_sec )
                            {
                                //
                                v_step = 2;
                                v_rc2 = v_rc;
                                //
                                ::localtime_r( &v_wi.m_ts.tv_sec, &v_tm );
                                ::strftime( v_ln, sizeof(v_ln), "%H:%M", &v_tm );
                                v_drc = print_ttf(v_scr, v_ln, FNT, FNT_SIZE_FORECAST, v_clr, v_rc2 );
                                v_rc2.y += v_drc.h;
                                //
                                ::strftime( v_ln, sizeof(v_ln), "%d.%m", &v_tm );
                                print_ttf(v_scr, v_ln, FNT, FNT_SIZE_FORECAST, v_clr, v_rc2 );
                                v_rc2.y += v_drc.h;
                                //
                                v_icon_path = g_owm.get_icon( v_wi.m_icon.c_str() );
                                //
                                if ( v_icon_path )
                                {
                                    //
                                    v_icon = IMG_Load( v_icon_path );
                                    //
                                    SDL_BlitSurface( v_icon, 0, v_scr, &v_rc2 );
                                    v_rc2.y += 50;
                                }
                                //
                                ::sprintf( v_ln, "%.1f°C", v_wi.m_temp );
                                print_ttf(v_scr, v_ln, FNT, FNT_SIZE_FORECAST, v_clr, v_rc2 );
                                v_rc2.y += v_drc.h;
                                //
                                ::sprintf( v_ln, "%d мм", (int)((v_wi.m_pressure * MM_HG) + 0.5) );
                                print_ttf(v_scr, v_ln, FNT, FNT_SIZE_FORECAST, v_clr, v_rc2 );
                                v_rc2.y += v_drc.h;
                                //
                                ::sprintf( v_ln, "%d м/с", (int)(v_wi.m_windspeed + 0.5) );
                                print_ttf(v_scr, v_ln, FNT, FNT_SIZE_FORECAST, v_clr, v_rc2 );
                                //
                                v_rc.x += 53;
                                //
                                ++k;
                            }
                        }
                    }
                }
                //
                SDL_UpdateRect(v_scr,0,0,0,0);
                SDL_Delay(8000);
            }
        }
        //
        for ( int w = 0; w < 300 && !g_stop_flag; ++w )
        {
            //
            ::sleep( 1u );
        }
    }
    //
    if ( v_icon )
    {
        //
        SDL_FreeSurface( v_icon );
    }
    //
    if ( v_bg )
    {
        //
        SDL_FreeSurface( v_bg );
    }
    //
    SDL_Delay(1000);
    //
    return 0;
}


SDL_Rect print_ttf(SDL_Surface *sDest, const char* message, const char* font, int size, SDL_Color color, SDL_Rect dest)
{
    int w = 0, h = 0;
    SDL_Rect v_result;
    
    //
    ::bzero( &v_result, sizeof(v_result) );
    //
    TTF_Font *fnt = TTF_OpenFont(font, size);
    //
    TTF_SizeUTF8( fnt, message, &w, &h );
    //
    SDL_Surface *sText = TTF_RenderUTF8_Blended( fnt, message, color);
    //
    SDL_BlitSurface( sText,NULL, sDest, &dest );
    //
    SDL_FreeSurface( sText );
    //
    TTF_CloseFont( fnt );
    //
    v_result.w = w;
    v_result.h = h;
    //
    return v_result;
}
